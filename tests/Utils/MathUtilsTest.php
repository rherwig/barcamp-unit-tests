<?php declare(strict_types=1);

namespace App\Tests\Utils;

use App\Utils\MathUtils;
use PHPUnit\Framework\TestCase;


class MathUtilsTest extends TestCase
{
    /**
     * @test returns correct sum
     */
    public function testReturnsCorrectSum()
    {
        $sum = MathUtils::add(3, 5);
        $this->assertEquals(8, $sum);
    }
}
