# Barcamp: Unit Tests
This repository serves as a demonstration for the SHOPMACHER Barcamp about
unit tests.

## Requirements
- PHP >= 7.2
- composer
- NodeJS >= 8
- npm >= 5

## Setup
After cloning the repository, execute the following commands, to ensure
all dependencies are present.

```bash
$ composer install
$ npm install
```

## Executing tests
There are unit tests for PHP, plain JavaScript as well as VueJS.

### PHP
PHP tests are located inside the `tests/` directory and can be executed by running
the following command:

```bash
$ php bin/phpunit
```

### JavaScript
JavaScript tests are located next to their respective feature inside the
`assets/` directory and can be executed by running

```bash
$ npm test
```

## License
MIT
