module.exports = {
    moduleFileExtensions: [
        'js',
        'json',
        'vue',
    ],
    transform: {
        '.*\\.(vue)$': '<rootDir>/node_modules/vue-jest',
        '^.+\\.js$': '<rootDir>/node_modules/babel-jest',
    },
    moduleNameMapper: {
        '^@/(.*)$': '<rootDir>/assets/vue/$1',
    },
    collectCoverageFrom: [
        '<rootDir>/assets/**/*.{js,vue}',
        '!**/node_modules/**',
    ],
};
