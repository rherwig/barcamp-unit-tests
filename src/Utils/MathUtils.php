<?php declare(strict_types=1);

namespace App\Utils;


class MathUtils
{
    /**
     * Adds two numbers and returns the result.
     *
     * @param int $a Number 1
     * @param int $b Number 2
     * @return int Sum of $a and $b
     */
    public static function add(int $a, int $b): int
    {
        return $a + $b;
    }
}
