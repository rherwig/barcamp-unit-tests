const { resolve } = require('path');
const WebpackBar = require('webpackbar');
const { VueLoaderPlugin } = require('vue-loader');

const context = resolve(__dirname, '../../');

module.exports = {
    entry: resolve(context, 'assets/index.js'),
    output: {
        path: resolve(context, 'public/assets'),
        filename: 'app.bundle.js',
        publicPath: '/assets/',
        chunkFilename: '[name].chunk.js',
    },
    resolve: {
        extensions: ['.js', '.vue'],
        alias: {
            '@': resolve(context, 'assets/vue'),
            'vue$': 'vue/dist/vue.esm.js',
        },
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: 'babel-loader',
            },
            {
                test: /\.vue$/,
                exclude: /node_modules/,
                use: 'vue-loader',
            },
        ],
    },
    plugins: [
        new WebpackBar(),
        new VueLoaderPlugin(),
    ],
};
