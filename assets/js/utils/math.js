/**
 * Adds two numbers and returns the result.
 *
 * @param a {number} Number 1
 * @param b {number} Number 2
 * @returns {number} Sum of a and b
 */
export const add = (a, b) => {
    return a + b;
};
