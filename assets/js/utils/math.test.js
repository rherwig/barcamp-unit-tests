import * as math from './math';

describe('math.js', () => {
    test('exposes an add method', () => {
        expect(typeof math.add).toBe('function');
    });

    test('returns the correct sum for valid numbers', () => {
        const result = math.add(23, 53);
        expect(result).toBe(76);
    });

    // test('returns NaN if one arguments is not of type number', () => {
    //     const result = math.add('test', 53);
    //     expect(result).toBeNaN();
    // });
});
