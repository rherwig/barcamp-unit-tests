import Vue from 'vue';

import App from './components/App';

new Vue({
    el: '#vue-root',
    render: h => h(App),
});
