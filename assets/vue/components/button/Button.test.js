import { mount } from '@vue/test-utils';

import Button from './Button';

describe('Button.vue', () => {
    test('renders an html button', () => {
        const wrapper = mount(Button);
        const hasButton = wrapper.contains('button');

        expect(hasButton).toBe(true);
    });

    test('is not disabled by default', () => {
        const wrapper = mount(Button);
        const isDisabled = wrapper.props('disabled');

        expect(isDisabled).toBe(false);
    });

    test('calls click handler', () => {
        const handler = jest.fn();
        const wrapper = mount(Button, {
            listeners: {
                click: handler,
            },
        });
        wrapper.find('button').trigger('click');

        expect(handler.mock.calls.length).toEqual(1);
    });
});
